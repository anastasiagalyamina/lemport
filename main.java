import java.util.ArrayList;
import java.util.Random;
import java.util.Hashtable;

//Алгоритм Лэмпорта 
public class main {	
	static int size = 100;
	static ArrayList<Process> processes = new ArrayList();
	static Random rnd = new Random();
	
	public static void main(String[] args) {
		createProcesses();
	}	
	
	// если какой-то процесс входит в КО, то передает информацию всем остальным процессам
	public static void enterCS(int Id, int time) {		
		for (int i = 0; i < size; i++) {		
			// другие процессы помещают процесс в свои локальные очереди      
			processes.get(i).addProcess(Id, time);
		}
	}

	// проверка, что критическая область свободна   	
	static Hashtable<Integer, Integer> checkEmptiness() {
		Hashtable<Integer, Integer> table = new Hashtable<Integer, Integer>();
		for (int i = 0; i < size; i++) {
			// возвращает все процессы, находящиеся в критической области (возвращает словарь Id - время)  
			if(processes.get(i).isInCS() != -1) {
				Process process = processes.get(i);
				table.put(process.getId(), process.getTime());
			}					
		}
		return table;
	}
	
	static void createProcesses() {
		for(int i = 0; i < size; i ++) {
			int diff = rnd.nextInt(3);
			processes.add(new Process(i, diff));
			}
	}
}
