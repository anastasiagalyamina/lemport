import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class Process {
	boolean inCS = false;
	boolean request_CS = false;
	//  локальная очередб
	Dictionary <Integer, Integer> queue = new Hashtable<>();
	int time = 0;
	int enterTime;
	int Id;
	boolean blocked = false;	
	Timer timer = new Timer();
    timer.schedule(event, diff);
    
	public Process(int Id, int diff) {
		//  чтобы было рассогласование по времени, в каждом процессе есть отставание     
		time = diff;
		this.Id = Id;
		do_something();
	}    
    
    TimerTask event = new TimerTask() {
    	@Override  
        public void run() {
    		Set k = queue.keys();
            for (int i = 0; i < k.size(); i++) {
            	// процессы, которые уже вошли в критическую область, удаляются 
            	if(queue.get(k[i]) < time) {
            		queue.remove(k[i]);
            	}
            }
            //  если у процесса наименьшая временная метка, то он заходит в КО     
            int min = queue[0];
            for(int i = 0; i < k.size(); i++) {
            	if(queue.get(k) < min) {
            		min = queue.get(k);
            	}
            }            
            if(queue.get(Id) = min) {
            	requestCS(time);
            }
            time ++;
        }
    }; 	
	
	public int getId() {
		return Id;
	}	
	
	// если процесс находится в критической области, то возвращает время, если нет, то -1
	public int isInCS() {
		if(inCS)
			return enterTime;
		else
			return -1;
		}
	

	public int getTime() {
		return enterTime;
	}
		
	//какие-то действия внутри процесса   
	void do_something() {
		while(true) {			
			addRequest();
		}	
	}	
	
	void block() {
		blocked = true;	
		
	}
	
	public void addRequest() {
		queue.put(Id, time);
	}
	
	
	public void requestCS(int time) {
		// блокируется до получения разрешения  
		block();
		enterTime = time;
		main.checkEmptiness();
		Hashtable<Integer, Integer> processes = main.checkEmptiness();
		// если ни один процесс не вернул время, то критическая область свободна    
		if(processes.isEmpty()) {
			main.enterCS(Id, time);
		}
		// если процессы сообщили, что находятся в КО, то они добавляются в локальную очередь     
		else {
			Set k = processes.keys();
			for(int i = 0; i < k.size(); i++) {
				queue.put(k, processes.get(k));	
			}		
		}
		blocked = false;
	}
	
	// получает информацию, что процесс вошел в критическую область и добавляет в свою очередь           
	void addProcess(int Id, int time) {		
		queue.put(Id, time);
	}	
		  
	void release_CS() {
			request_CS = false;
			inCS = false;
		}
		
}
